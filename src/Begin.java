import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Istolla on 2/26/2017.
 */
public class Begin {
    public static void main(String... args) {
        long startTime = System.currentTimeMillis();
        solution();
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Run time = " + totalTime);
    }

    public static void solution() {
        BigInteger n = BigInteger.ONE;
        BigInteger result;
        boolean solutionFound = false;
        while (!solutionFound) {
            result = n.divide(phi3(n));
            if (result.compareTo(BigInteger.valueOf(10)) > 0)
                solutionFound = true;
            else
                n = n.add(BigInteger.ONE);
        }
        System.out.println("n = " + n);
        System.out.println("phi(n)= " + phi3(n));
    }

    public static BigInteger phi(BigInteger n) {
        List<PrimeFactor> factors = getPrimeFactors(n);
        BigInteger result = factors.stream().map(PrimeFactor::getPhi)
                                   .reduce(BigInteger.ONE, (a, b) -> a.multiply(b));
        return result;
    }

    public static BigInteger phi2(BigInteger n) {
        BigInteger result = BigInteger.ONE;
        for (BigInteger i = BigInteger.valueOf(2); i.compareTo(n) < 0; i = i.add(BigInteger.ONE)) {
            if (n.gcd(i).compareTo(BigInteger.ONE) == 0)
                result = result.add(BigInteger.ONE);
        }
        return result;
    }

    public static BigInteger phi3(BigInteger n) {
        BigInteger result = n, prime = BigInteger.ONE.nextProbablePrime();
        if (n.isProbablePrime(10))
            result = n.subtract(BigInteger.ONE);
        else {
            while (prime.pow(2).compareTo(n) < 0 || prime.pow(2).compareTo(n) == 0) {
                if (n.mod(prime).compareTo(BigInteger.ZERO) == 0) {
                    while (n.mod(prime).compareTo(BigInteger.ZERO) == 0)
                        n = n.divide(prime);
                    result = result.subtract(result.divide(prime));
                }
                prime = prime.nextProbablePrime();
            }
            if (n.compareTo(BigInteger.ONE) > 0)
                result = result.subtract(result.divide(n));
        }
        return result;
    }

    private static boolean isPrime(int num) {
        if (num < 2)
            return false;
        if (num == 2)
            return true;
        if (num % 2 == 0)
            return false;
        for (int i = 3; i * i <= num; i += 2)
            if (num % i == 0)
                return false;
        return true;
    }

    private static List<PrimeFactor> getPrimeFactors(BigInteger n) {
        List<PrimeFactor> result = new ArrayList<>();
        BigInteger prime = BigInteger.ONE;
        prime = prime.nextProbablePrime();
        if (n.compareTo(BigInteger.ZERO) > 0) {
            if (n.compareTo(BigInteger.ONE) > 0) {
                if (n.isProbablePrime(5))
                    result.add(new PrimeFactor(n, 1));
                else {
                    while (prime.compareTo(n) < 0) {
                        if (n.mod(prime) == BigInteger.ZERO)
                            result.add(new PrimeFactor(prime, factorPower(prime, n)));
                        prime = prime.nextProbablePrime();
                    }
                }
            }
        }
        return result;
    }

    private static int factorPower(int factor, int number) {
        int result = 0;
        int temp = number;
        while (temp % factor == 0 && temp > 0) {
            temp /= factor;
            result++;
        }
        return result;
    }

    private static int factorPower(BigInteger factor, BigInteger number) {
        int result = 0;
        BigInteger temp = number;
        while (temp.mod(factor) == BigInteger.ZERO && temp.compareTo(BigInteger.ZERO) > 0) {
            temp = temp.divide(factor);
            result++;
        }
        return result;
    }

    private static class PrimeFactor {
        BigInteger factor;
        Integer exponent;

        public PrimeFactor(BigInteger a, Integer b) {
            setFactor(a);
            setExponent(b);
        }

        public void setFactor(BigInteger factor) {
            this.factor = factor;
        }

        public BigInteger getFactor() {
            return factor;
        }

        public void setExponent(Integer exponent) {
            this.exponent = exponent;
        }

        public Integer getExponent() {
            return exponent;
        }

        public BigInteger getPhi() {
            BigInteger phi = BigInteger.ONE;
            phi = (factor.subtract(BigInteger.ONE).multiply(factor.pow(exponent - 1)));
            return phi;
        }
    }
}
